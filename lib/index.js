const fs = require('fs');
const commander = require('commander');
const chalk = require('chalk');
const { performance } = require('perf_hooks');

const { checkFile } = require('./checkFile');
const parseFolder = require('./parseFolder');

const excludedFolders = ['node_modules'];

commander
    .version(require('../package.json', '-V, --version', 'show version').version)
    .requiredOption('-i, --item <item>', 'folder or file to examine  -- required!')
    .option('-x, --exclude [exclude]', 'folders to exclude comma separated.')
    .option('-d, --depth [depth]', 'depth, default is Infinity')
    .option('-p, --perf [perf]', 'show time lapsed')
    .option('-s, --save [save]', 'save result, provide filename')
    .on('--help', () => {
        console.log('');
        console.log(chalk.green('[information]'));
        console.log(chalk.blue(' * This is ' + chalk.blue.bold('NOT') + ' a linter!\n'));
        console.log(' - node_modules is always ignored');
        console.log(' - only .js, .jsx .mjs, .ts, .tsx files ares scanned');
        console.log(' - all files are processed.\n');
    })
    .parse(process.argv);

const { item, perf, save, exclude, depth = Infinity } = commander;

exclude && exclude.split(',').forEach(exclusion => excludedFolders.push(exclusion));

fs.lstat(item, (err, stats) => {
    const start = perf && performance.now();
    let res = {};
    if (err) {
        console.error(chalk.red('[Error]::'), chalk.yellow(err.message));
        process.exit(1);
    }
    if (stats.isFile()) {
        res = { parent: [checkFile(item, stats)], fileCount: 1 };
    }
    else if (stats.isDirectory()) {
        res = parseFolder({folder: item, parent: res, depth, exclusions: excludedFolders});
    }
    else {
        console.log(chalk.red('[Error]:: ', chalk.yellow('item is not of type directory or file!')));
    }
    const end = perf && performance.now();
    const stringified_result = JSON.stringify({...res.parent, 'execution-time': perf ? `${(end - start).toFixed(2)} ms`: undefined, 'file count': res.fileCount }, null, 2);
    if (save) {
        fs.writeFileSync(save, stringified_result);
    } else {
        console.log(stringified_result);
    }
});
