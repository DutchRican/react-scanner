const fs = require('fs');
const path = require('path');
const prettyBytes = require('pretty-bytes');
const allowedExtensions = ['.js', '.jsx', '.ts', '.tsx', '.mjs'];
const CLASS_COMP = 'Class Component';
const FUNC_COMP = 'Functional Component';

const checkFile = (item, stats) => {
    const fileSize = prettyBytes(stats.size);
    if (!allowedExtensions.includes(path.extname(item))) {
        return { item, fileSize }
    }
    const contents = fs.readFileSync(item, 'utf8').split(/\n|\r\n|\r/);
    const imports = contents.filter(line => line.match(/(^import .+)|((.+\s)?=\s?require(.+))$/));
    const reactInformation = checkReactInformation(imports, contents);
    const usesRedux = reactInformation ? imports.some(line => line.match(/import {\s?connect\s?}/)) : undefined;
    const fileInformation = {
        item,
        lineCount: contents.length,
        fileSize,
        'react information': reactInformation && { ...reactInformation },
        'connects Redux': usesRedux
    }
    return { ...fileInformation }
};

const checkReactInformation = (imports, contents) => {
    const isReact = imports.some(line => line.includes("'react'"));
    if (!isReact) return undefined;
    const usesClasses = contents.filter(line => line.match(/class.+?extends (React.)?Component/));
    const typeOfComponent = usesClasses.length ? CLASS_COMP : FUNC_COMP;
    const hooks = [];
    typeOfComponent === FUNC_COMP && imports.forEach(line => {
        const matches = line.match(/use(Effect|State|Reducer|Context|Callback|Memo|Ref)/g);
        matches && matches.forEach(match => hooks.push(match));
    });
    return { typeOfComponent, 'hooks used': hooks.length ? hooks : undefined };
}

module.exports = { checkFile, checkReactInformation };


