const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const { checkFile } = require('./checkFile');

let fileCount = 0;
let excludedFolder;
const parseFolder = ({folder, parent, depth, exclusions, level = -1}) => {
    excludedFolder = exclusions ? exclusions : excludedFolder;
    const items = fs.readdirSync(folder);
    parent.files = parent.files || [];
    level++;
    for (item of items) {
        const fullPath = path.join(folder, item);
        const stats = fs.lstatSync(fullPath);
        try {
            if (stats.isFile()) {
                parent.files.push(checkFile(fullPath, stats));
                fileCount += 1;
            }

            if (stats.isDirectory() && level < depth && !excludedFolder.includes(item)) {

                parent[item] = parent[item] || {};
                parseFolder({folder: fullPath, parent: parent[item], depth, level});
            }

        } catch (err) { console.log(chalk.red('[error::] '), chalk.yellow(err.message)) }
    }
    return { parent, fileCount }
}

module.exports = parseFolder;
