const { checkFile, checkReactInformation } = require('../lib/checkFile');
const fs = require('fs');
const path = require('path');

describe('checkFile - file', () => {
    describe('checkFile', () => {
        it('should find React import in the file', () => {
            const [filePath, stats] = getFilePathAndStats('test-data/import.jsx');
            const res = checkFile(filePath, stats);
            expect(res['react information']).toBeDefined();
        });

        it('should find React require in the file', () => {
            const [filePath, stats] = getFilePathAndStats('test-data/require.js');
            const res = checkFile(filePath, stats);
            expect(res['react information']).toBeDefined();
        });

        it('should only return item and fileSize for other file-type', () => {
            const [filePath, stats] = getFilePathAndStats('test-data/otherfile.txt');
            const res = checkFile(filePath, stats);
            expect(res).toEqual({ item: filePath, fileSize: '102 B' });
        });

        it('should parse file without react', () => {
            const [filePath, stats] = getFilePathAndStats('test-data/no-react.ts');
            const res = checkFile(filePath, stats);
            expect(res).toEqual({ item: filePath, lineCount: 1, fileSize: '20 B', "react information": undefined, "connects Redux": undefined });
        });

        it('should find hooks', () => {
            const [filePath, stats] = getFilePathAndStats('test-data/react-hooks.tsx');
            const res = checkFile(filePath, stats);
            expect(res).toEqual({
                item: filePath, lineCount: 4, 'react information': {
                    typeOfComponent: 'Functional Component',
                    'hooks used': ['useEffect']
                }, fileSize: '82 B', 'connects Redux': false
            });
        });
    });
    describe('checkReactInformation', () => {
        const classComponent = 'class Test extends React.Component';
        const reactImport = 'import React from \'react\';';
        const reactRequire = 'const React = require(\'react\');';
        const hooksImport = 'import React, { useMemo } from \'react\';';
        it('should return undefined if not react', () => {
            const res = checkReactInformation([], ['']);
            expect(res).toBeUndefined();
        });
        it('should return Class type of Component', () => {
            const res = checkReactInformation([reactImport], [classComponent]);
            expect(res).toEqual({typeOfComponent: 'Class Component', "hooks used": undefined});
        });
        it('should return Functional type Component', () => {
            const res = checkReactInformation([hooksImport], [hooksImport]);
            expect(res).toEqual({typeOfComponent: 'Functional Component', "hooks used": ['useMemo']});
        });
        it('should return identify a required import', () => {
            const res = checkReactInformation([reactRequire], []);
            expect(res).toEqual({typeOfComponent: 'Functional Component', "hooks used": undefined});
        });
    });
});

const getFilePathAndStats = (item) => {
    const filePath = path.join(__dirname, item);
    const stats = fs.lstatSync(filePath);
    return [filePath, stats];
}